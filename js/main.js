import asideBar from "./aside/aside.js";
import burgerToggle from "./burger/burger.js";
import content from "./content/contentController.js";
import slider from "./slider/sliderController.js";
import filter from "./filter/filterController.js";

const state = {}
// Только для тестирования - потом удалить
window.state = state

asideBar();
burgerToggle();
slider();
content(state);
filter(state);






