export default class Lectures {
    constructor() {
        this.lectures = [],
        this.data = {}
    }

    addData(data) {

        
        data.forEach((item) => {

            let id = 1;

            if (this.lectures.length > 0) {
                id = this.lectures[this.lectures.length - 1]["id"] + 1
            }

            const lecture = {
                title: item.title,
                description: item.description,
                date: item.date,
                image: item.image,
                id: id,
                label: item.label
            }

            this.lectures.push(lecture)

        })

        return this.lectures

    }


}

