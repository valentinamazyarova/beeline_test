export const elements = {
    mainContant: document.querySelector('#mainContant'),
    lectures: document.querySelectorAll('[data-group]')
}


export function titlestoUpperCase() {
    elements.mainContant.querySelectorAll('[data-title]').forEach(function (item) {
        item.innerText = item.innerText.toUpperCase();
    });
}

export function subtitlesCut() {
    elements.mainContant.querySelectorAll('[data-subtitle]').forEach(function (item) {
        if (item.innerText.length >= 20) {
            item.innerText = item.innerText.slice(0, 21).concat("...");
        }
    })
}

export function renderContainerAllLectures() {

    const markup = `
                    <section data-section="section">
                        <div class="section__firstline" data-container="firstline">
                            <div class="section__firstline_right">
                                <p data-level>Все лекции на нашем сайте</p>
                                <h1>ALL LECTURES</h1>
                            </div>
                            <div class="section__firstline_left" data-container="firstline_left">
                                <button class="btn_main btn_one" data-filter="html">HTML</button>
                                <button class="btn_main btn_one" data-filter="css">CSS</button>
                                <button class="btn_main btn_one" data-filter="javascript">Javascript</button>
                                <button class="btn_main btn_one" data-filter="react">React</button>
                                <button class="btn_main btn_one btn_main--active" data-filter="all">8 лекции</button>
                                <button class="btn_slide " data-btn="left">
                                    <img class="slide_left" src="../img/arrow.svg" alt="">
                                </button>
                                <div class="wrapperForBtn">
                                    <button class="btn_slide " data-btn="right" >
                                        <img class="slide_right" src="../img/arrow.svg" alt="">
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="section__secondline">
                            <div class="slider" style="left: 0px" id="allLectures">
                            
                            </div>
                        </div>
                        `

    elements.mainContant.insertAdjacentHTML('afterbegin', markup)
}


export function getDataLectures(){

    const data = [];

    elements.lectures.forEach((item) => {
        const lecture = {
            title: '',
            description: '',
            date: '',
            image: '',
            id:'',
            label:''
        }

        lecture.image = item.querySelector('img').src;
        lecture.title = item.querySelector('[data-title]').innerText;
        lecture.description = item.querySelector('[data-subtitle]').innerText;
        lecture.label = item.dataset.group;
        lecture.date = item.querySelector('[data-date]').innerText;

        data.push(lecture)
    })

    return data
}

export function renderLectures(arrLectures){

    arrLectures.forEach((obj) => {

        const containerAllLectures = document.querySelector('#allLectures')

        const markup = `
                        <div class="section__secondline__item" data-group="${obj.label}">
                            <div class="section__secondline__item_inner1">
                                <img src="${obj.image}" alt="">
                                <button class="btn_main btn_two ">Лекция</button>
                            </div>
                            <div class="section__secondline__item_inner2">
                                <h2 data-title>${obj.title}</h2>
                                <p data-subtitle="CSS Grid">${obj.description}</p>
                            </div>
                            <div class="section__secondline__item_inner3">
                                <p data-date>${obj.date}</p>
                                <button class="btn_main btn_three">Смотреть</button>
                            </div>
                        </div>
        `
        containerAllLectures.insertAdjacentHTML('beforeend', markup);
        

    })

    
}

export function changeTextBtn(num){
    document.querySelector('[data-filter="all"]').textContent = num + ' лекции';
}






