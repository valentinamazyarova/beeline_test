import * as view from './contentView.js';
import Model from './contentModel.js'

export default function (state) {

    window.onload = function () {
        const model = new Model()
        view.renderContainerAllLectures();
        const dataLectures = view.getDataLectures();
        state.lectures = model.addData(dataLectures);
        view.renderLectures(state.lectures);
        view.titlestoUpperCase();
        view.subtitlesCut();
        view.changeTextBtn(state.lectures.length)

    }



}



