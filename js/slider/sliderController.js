import * as view from './sliderView.js'

export default function slider(){

    view.elements.mainContainer.addEventListener('click', function(e){
        if(e.target.closest('[data-btn="left"]')){
            view.btnLeft(e.target)
        } else if(e.target.closest('[data-btn="right"]')){
            view.btnRight(e.target)
        }
    })
}