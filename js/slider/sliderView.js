
export const elements = {
    mainContainer: document.querySelector('#mainContant')
};

export function btnLeft(target) {

    const slider = target.closest('[data-container="firstline"]').nextElementSibling.querySelector('.slider');
    // const level = target.closest('[data-container="firstline"]').querySelector('[data-level]');

    let offset = Number((slider.style.left).slice(0, -2));
    offset +=  265 * 4;
    if (offset >= 265 * 4) offset = 0;
    slider.style.left = offset + 'px';

    // level.textContent = "Базовый уровень";

};

export function btnRight(target) {

    const slider = target.closest('[data-container="firstline"]').nextElementSibling.querySelector('.slider');
    // const level = target.closest('[data-container="firstline"]').querySelector('[data-level]');
    const itemsLectures = slider.querySelectorAll('[data-group]');

    let offset = Number((slider.style.left).slice(0, -2));
    
    let sumContainersSlider;
    if(itemsLectures.length % 4 !== 0){
        sumContainersSlider = Math.floor(itemsLectures.length / 4) + 1;
    } else {
        sumContainersSlider = itemsLectures.length / 4;
    }

    offset -= 265 * 4;
    if(offset < -(sumContainersSlider * 256 * 4)) offset = 0
    slider.style.left = offset + 'px';

    // level.textContent = "Продвинутый уровень";
    
};