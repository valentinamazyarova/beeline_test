
export default function asideBar() {
    let asideBar = document.querySelector('#aside-bar')

    asideBar.addEventListener("click", openList)

    function openList(event) {

        if (event.target.closest("[data-openList]")) {
            event.target.nextElementSibling.classList.toggle('dop3');
            event.target.querySelector("[data-arrow]").classList.toggle('rotate');
        }

    }
}


