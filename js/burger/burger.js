export default function burgerToggle() {
    let burger = document.querySelector('.burger');
    let aside = document.querySelector('.aside');

    burger.addEventListener('click', toggleMenu)

    function toggleMenu() {
        aside.classList.toggle('burger_active')
    }
}






