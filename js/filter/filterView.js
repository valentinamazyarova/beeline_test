
export const elements = {
    mainContant: document.querySelector('#mainContant'),
}

export function renderFilterLectures(filterData){

    document.querySelector('#allLectures').innerHTML = "";

    filterData.forEach((obj) => {

        const containerAllLectures = document.querySelector('#allLectures')

        const markup = `
                        <div class="section__secondline__item" data-group="${obj.label}">
                            <div class="section__secondline__item_inner1">
                                <img src="${obj.image}" alt="">
                                <button class="btn_main btn_two ">Лекция</button>
                            </div>
                            <div class="section__secondline__item_inner2">
                                <h2 data-title>${obj.title}</h2>
                                <p data-subtitle="CSS Grid">${obj.description}</p>
                            </div>
                            <div class="section__secondline__item_inner3">
                                <p data-date>${obj.date}</p>
                                <button class="btn_main btn_three">Смотреть</button>
                            </div>
                        </div>
        `
        containerAllLectures.insertAdjacentHTML('beforeend', markup);
        

    })

}

export function changeBtnActive(target){
    document.querySelector('.btn_main--active').classList.remove('btn_main--active')
    target.classList.add('btn_main--active')
}

export function changeTextBtn(num){
    document.querySelector('[data-filter="all"]').textContent = num + ' лекции';
}

export function resetOffsetSlider(){
    document.querySelector('#allLectures').style.left = "0px"
}

export function titlestoUpperCase() {
    elements.mainContant.querySelectorAll('[data-title]').forEach(function (item) {
        item.innerText = item.innerText.toUpperCase();
    });
}

export function subtitlesCut() {
    elements.mainContant.querySelectorAll('[data-subtitle]').forEach(function (item) {
        if (item.innerText.length >= 20) {
            item.innerText = item.innerText.slice(0, 21).concat("...");
        }
    })
}

