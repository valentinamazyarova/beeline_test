import * as model from './filterModel.js'
import * as view from './filterView.js'

export default function filter(state) {

    view.elements.mainContant.addEventListener('click', function (e) {

        if (e.target.closest('[data-filter]')) {
            state.filterData = model.getFilterData(e.target.dataset.filter, state.lectures);
            view.renderFilterLectures(state.filterData);
            view.changeBtnActive(e.target);
            view.resetOffsetSlider();
            view.changeTextBtn(state.filterData.length);
        }

    })

    view.titlestoUpperCase();
    view.subtitlesCut();
}

