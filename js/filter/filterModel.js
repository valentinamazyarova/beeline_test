

export function getFilterData(filterText, lectures){

    let filterData = lectures.filter((lecture) => {
        if(filterText !== 'all'){
          return lecture.label == filterText  
        }  else {
            return lecture
        }
    })

    return filterData
}